package the_fireplace.unlogic.worldgen;

import the_fireplace.unlogic.worldgen.UnLogicWorldChunkManager;
import net.minecraft.world.World;
import net.minecraft.world.WorldType;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraft.world.biome.WorldChunkManager;

public class UnLogicWorldType extends WorldType{

	public UnLogicWorldType(String name) {
		super(name);
	}
@Override
public WorldChunkManager getChunkManager(World world)
{
	return new UnLogicWorldChunkManager();
}
}

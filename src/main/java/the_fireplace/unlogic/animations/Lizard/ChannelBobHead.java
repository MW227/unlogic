package the_fireplace.unlogic.animations.Lizard;

import the_fireplace.fireplacecore.MCACommonLibrary.animation.Channel;
import the_fireplace.fireplacecore.MCACommonLibrary.animation.KeyFrame;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Quaternion;
import the_fireplace.fireplacecore.MCACommonLibrary.math.Vector3f;


public class ChannelBobHead extends Channel {
	public ChannelBobHead(String _name, float _fps, int _totalFrames, byte _mode) {
		super(_name, _fps, _totalFrames, _mode);
	}

	@Override
	protected void initializeAllFrames() {
KeyFrame frame0 = new KeyFrame();
frame0.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.02617695F, 0.99965733F));
frame0.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(0, frame0);

KeyFrame frame1 = new KeyFrame();
frame1.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.017452406F, 0.9998477F));
frame1.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(1, frame1);

KeyFrame frame2 = new KeyFrame();
frame2.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.008726535F, 0.9999619F));
frame2.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(2, frame2);

KeyFrame frame3 = new KeyFrame();
frame3.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.0F, 1.0F));
frame3.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(3, frame3);

KeyFrame frame4 = new KeyFrame();
frame4.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, -0.008726535F, 0.9999619F));
frame4.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(4, frame4);

KeyFrame frame5 = new KeyFrame();
frame5.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.0F, 1.0F));
frame5.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(5, frame5);

KeyFrame frame6 = new KeyFrame();
frame6.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.008726535F, 0.9999619F));
frame6.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(6, frame6);

KeyFrame frame7 = new KeyFrame();
frame7.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.017452406F, 0.9998477F));
frame7.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(7, frame7);

KeyFrame frame8 = new KeyFrame();
frame8.modelRenderersRotations.put("Head", new Quaternion(0.0F, 0.0F, 0.02617695F, 0.99965733F));
frame8.modelRenderersTranslations.put("Head", new Vector3f(0.0F, 0.0F, 0.0F));
keyFrames.put(8, frame8);

}
}
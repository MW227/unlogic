package the_fireplace.unlogic.items.weapons;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.ItemSword;

public class CactusSword extends ItemSword{

	public CactusSword(ToolMaterial p_i45356_1_) {
		super(p_i45356_1_);
		setTextureName("unlogic:cactus_sword");
		setCreativeTab(unbase.TabUnLogic);
	}

}

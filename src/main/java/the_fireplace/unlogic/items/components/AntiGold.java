package the_fireplace.unlogic.items.components;

import java.util.List;

import the_fireplace.unlogic.unbase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public class AntiGold extends Item{
	public AntiGold(){
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("AntiGold");
	}
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player,
            List list, boolean par4) {
		list.add("To create, throw "+StatCollector.translateToLocal("item.UnlogicEnderPearlDust.name")+",");
		list.add(StatCollector.translateToLocal("item.redstone.name")+", "+StatCollector.translateToLocal("item.ingotGold.name")+",");
		list.add("and "+StatCollector.translateToLocal("item.yellowDust.name"));
		list.add("on the ground together.");
	}
}

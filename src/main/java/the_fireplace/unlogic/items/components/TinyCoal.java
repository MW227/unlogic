package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;

public class TinyCoal extends Item{
	public TinyCoal(){
		setTextureName("unlogic:TinyCoal");
		setUnlocalizedName("TinyCoal");
		setCreativeTab(unbase.TabUnLogic);
	}
}

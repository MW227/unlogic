package the_fireplace.unlogic.items.components;

import java.util.List;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.entities.EntityNeedle;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

public class Needle extends Item {
	public Needle(){
        this.setCreativeTab(unbase.TabUnLogic);
        this.setTextureName("unlogic:Needle");
	}
	@Override
	public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_)
    {
        if (!p_77659_3_.capabilities.isCreativeMode)
        {
            --p_77659_1_.stackSize;
        }

        p_77659_2_.playSoundAtEntity(p_77659_3_, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

        if (!p_77659_2_.isRemote)
        {
            p_77659_2_.spawnEntityInWorld(new EntityNeedle(p_77659_2_, p_77659_3_));
        }

        return p_77659_1_;
    }
	@Override
	public void addInformation(ItemStack itemStack, EntityPlayer player,
            List list, boolean par4) {
		list.add("To create, right click");
		list.add(StatCollector.translateToLocal("tile.cactus.name")+" with");
		list.add(StatCollector.translateToLocal("item.Canister.name")+".");
	}
}

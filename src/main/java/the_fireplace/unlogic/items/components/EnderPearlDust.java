package the_fireplace.unlogic.items.components;

import java.util.List;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.entities.EntityEnderPearlDust;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class EnderPearlDust extends Item{
	public EnderPearlDust(){
		setCreativeTab(unbase.TabUnLogic);
		setUnlocalizedName("UnlogicEnderPearlDust");
	}
	@Override
	public boolean hasCustomEntity(ItemStack stack)
    {
        return true;
    }
	@Override
	public Entity createEntity(World world, Entity location, ItemStack itemstack)
    {
		Entity newentity = new EntityEnderPearlDust(world, location.posX, location.posY, location.posZ, itemstack);
        return newentity;
    }
}

package the_fireplace.unlogic.items.components;

import the_fireplace.unlogic.unbase;
import net.minecraft.item.Item;

public class TinyCoalDust extends Item{
public TinyCoalDust(){
	setTextureName("unlogic:TinyCoalDust");
	setUnlocalizedName("TinyCoalDust");
	setCreativeTab(unbase.TabUnLogic);
}
}

package the_fireplace.unlogic.renderers;

import the_fireplace.unlogic.entities.F1repl4ce;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.model.ModelBlaze;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderBlaze;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

public class RenderF1repl4ce extends RenderBlaze{
	
	public static final ResourceLocation textureLocation = new ResourceLocation("unlogic:textures/entities/f1repl4ce_soul.png");
	public static ResourceLocation currentTexture = textureLocation;
	
	public RenderF1repl4ce(ModelBlaze model, float shadowSize) {
		super();
	}
	@Override
	protected ResourceLocation getEntityTexture(Entity par1Entity){
		//add when damaged, change texture
	return currentTexture;
	}
}

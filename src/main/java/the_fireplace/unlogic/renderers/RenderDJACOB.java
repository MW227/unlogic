package the_fireplace.unlogic.renderers;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelPig;
import net.minecraft.client.renderer.entity.RenderPig;
import net.minecraft.entity.passive.EntityPig;
import net.minecraft.util.ResourceLocation;

public class RenderDJACOB extends RenderPig{

	public static final ResourceLocation textureLocation = new ResourceLocation("unlogic:textures/entities/djacob.png");

	public RenderDJACOB(ModelBase model, ModelBase p_i1265_2_,
			float p_i1265_3_) {
		super(model, p_i1265_2_, p_i1265_3_);
	}
	@Override
	protected int shouldRenderPass(EntityPig p_77032_1_, int p_77032_2_, float p_77032_3_)
    {
        if (p_77032_2_ == 0 && p_77032_1_.getSaddled())
        {
            this.bindTexture(textureLocation);
            return 1;
        }
        else
        {
            return -1;
        }
    }
	@Override
	protected ResourceLocation getEntityTexture(EntityPig p_110775_1_)
    {
        return textureLocation;
    }
}

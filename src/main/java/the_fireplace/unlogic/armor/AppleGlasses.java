package the_fireplace.unlogic.armor;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.gui.GuiAppleGlassesLogo;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class AppleGlasses extends ItemArmor {

	public AppleGlasses(ArmorMaterial p_i45325_1_, int p_i45325_2_,
			int p_i45325_3_) {
		super(p_i45325_1_, p_i45325_2_, p_i45325_3_);
	}
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {
		if(stack.getItem() == unbase.AppleGlasses) {
			return "unlogic:textures/models/armor/apple_glasses.png";
		}
		return null;
	}
	@Override
	public void onArmorTick(World world, EntityPlayer player, ItemStack itemStack){
		if(player.getCurrentArmor(3) == new ItemStack(unbase.AppleGlasses)){
			GuiAppleGlassesLogo.texture3 = GuiAppleGlassesLogo.texture;
		}else{
			GuiAppleGlassesLogo.texture3 = GuiAppleGlassesLogo.texture2;
		}
	}
}

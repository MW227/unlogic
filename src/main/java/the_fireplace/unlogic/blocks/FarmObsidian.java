package the_fireplace.unlogic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class FarmObsidian extends WBlock{

	public FarmObsidian(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockName("FarmObsidian");
		setBlockTextureName("unlogic:FarmObsidian");
		setHardness(20);
	}

}

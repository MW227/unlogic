package the_fireplace.unlogic.blocks;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.config.UnLogicConfigValues;
import the_fireplace.unlogic.entities.tile.TileEntityMSTable;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
/*
 * A crafting Table that will be coded so you choose which mods' recipes to enable
 * @author The_Fireplace aka F1repl4ce
 */
public class MSTable extends WBlock{

	public MSTable(Material p_i45394_1_) {
		super(p_i45394_1_);
	}
	@Override
	public boolean onBlockActivated(World world, int i, int j, int k, EntityPlayer entityplayer, int par6, float par7, float par8, float par9) {
		super.onBlockActivated(world, i, j, k, entityplayer, par6, par7, par8, par9);
		// Drop through if the player is sneaking
		if (entityplayer.isSneaking()) {
			return false;
		}
		if (world.isRemote)
        {
            return true;
        }else{
			entityplayer.openGui(unbase.instance, 0/*unbase.WorkbenchGui.getId()*/, world, i, j, k);
			return true;
		}
	}
	@Override
    public boolean hasTileEntity(int metadata)
    {
		return true;
    }

	@Override
	public TileEntity createTileEntity(World world, int metadata) {
		return new TileEntityMSTable();
	}
}

package the_fireplace.unlogic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class LightConducter extends WBlock{

	public LightConducter(Material p_i45394_1_) {
		super(p_i45394_1_);
		this.useNeighborBrightness = true;
		this.setLightOpacity(-255);//negative values don't amplify it
	}
}

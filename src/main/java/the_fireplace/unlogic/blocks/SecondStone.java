package the_fireplace.unlogic.blocks;

import java.util.Random;

import the_fireplace.unlogic.unbase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.world.World;

public class SecondStone extends WBlock{

	public SecondStone(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockTextureName("minecraft:stone");
		setBlockName("UnLogicStone");
		setHardness(5);
	}
	@Override
	public Item getItemDropped(int p_149650_1_, Random p_149650_2_, int p_149650_3_)
    {
        return Item.getItemFromBlock(Blocks.cobblestone);
    }
	/*@Override
	public void breakBlock(World world, int x, int y, int z, Block p_149749_5_, int p_149749_6_){
     //world.createExplosion(null, x, y, z, 20, true);
     //EntityPlayer.addStat(unbase.tricked, 1);
    }*/
}

package the_fireplace.unlogic.blocks;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.config.UnLogicConfigValues;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class WBlock extends Block {

	protected WBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
	}
	@Override
	@SideOnly(Side.CLIENT)
    public CreativeTabs getCreativeTabToDisplayOn()
    {
		if(UnLogicConfigValues.DEVKEY == unbase.DEVKEY){
			return super.getCreativeTabToDisplayOn();
		}else{
			return null;
		}
    }
}

package the_fireplace.unlogic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.world.IBlockAccess;

public class RedGemBlock extends Block{

	public RedGemBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
		setBlockName("RedGemBlock");
		setBlockTextureName("unlogic:redGemBlock");
		setHardness(3);
	}
	@Override
	public boolean isBeaconBase(IBlockAccess worldObj, int x, int y, int z, int beaconX, int beaconY, int beaconZ)
    {
        return true;
    }
}

package the_fireplace.unlogic.blocks.internal;

import the_fireplace.unlogic.unbase;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockCrops;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;

public class QuartzCrop extends BlockCrops{

	@SideOnly(Side.CLIENT)
	private IIcon[] iconArray;
	public QuartzCrop() {
		super();
		this.setBlockName("quartzcrop");
	}
	
	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata){
		if(metadata < 5){
			if(metadata == 4){
				metadata = 3;
			}
			return iconArray[metadata >> 1];
		}
		return iconArray[4];
	}
	@Override
	public Item func_149866_i(){
		return unbase.QSeed;
	}
	@Override
	public Item func_149865_P(){
		return Item.getItemFromBlock(Blocks.quartz_block);
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister iconRegister){
		this.iconArray = new IIcon[5];
		
		for(int i = 0; i < this.iconArray.length; i++){
			this.iconArray[i] = iconRegister.registerIcon("unlogic:QCrop_" + (i+1));
		}
	}

}

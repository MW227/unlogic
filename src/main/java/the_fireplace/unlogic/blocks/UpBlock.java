package the_fireplace.unlogic.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.world.World;

public class UpBlock extends WBlock {

	public UpBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
	}
	@Override
    public void onEntityWalking(World world, int x, int y, int z, Entity entity) {
		//launch entity straight up, random strength
		//make it so if powered by redstone, it doesn't launch
	}
}

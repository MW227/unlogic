package the_fireplace.unlogic.blocks;

import the_fireplace.unlogic.unbase;
import the_fireplace.unlogic.config.UnLogicConfigValues;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class WBlockContainer extends BlockContainer {

	protected WBlockContainer(Material p_i45386_1_) {
		super(p_i45386_1_);
	}

	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return null;
	}
	@Override
	@SideOnly(Side.CLIENT)
    public CreativeTabs getCreativeTabToDisplayOn()
    {
		if(UnLogicConfigValues.DEVKEY == unbase.DEVKEY){
			return super.getCreativeTabToDisplayOn();
		}else{
			return null;
		}
    }
}
